from django.test import TestCase, Client
from .models import Todo


class TodoTestCase(TestCase):

    def setUp(self):
        Todo.create(title='Something witty', text='Come up with a wittier title for the next one')
        Todo.create(title='', text='What about a todo with no title?')
        Todo.create(title='Email from the school about internships', text='Im sorry')
        Todo.create(title='Feed the teacher', text='Leave some pizza, you assholes.')
        Todo.create(title='Not sure how many i have to do', text='It sure does work')
        Todo.create(title='Linting is fun', text='Linty lint')

    def test_default_order(self):
        first_item = Todo.objects.get(title='Something witty')
        last_item = Todo.objects.get(title='Not sure how many i have to do')
        assert first_item.rank < last_item.rank

    def test_highest_rank(self):
        linting_fun = Todo.objects.get(title='Linting is fun')
        assert linting_fun.rank == Todo.highest_rank

    def test_rerank_up(self):
        intern_horror = Todo.objects.get(title='Email from the school about internships')
        intern_horror.rerank(1)
        intern_horror.refresh_from_db()
        feed_teacher = Todo.objects.get(title='Feed the teacher')
        assert intern_horror.rank == 1
        assert feed_teacher.rank == 2

    def test_rerank_down(self):
        feed_teacher = Todo.objects.get(title='Feed the teacher')
        feed_teacher.rerank(4)
        feed_teacher.refresh_from_db()
        assert feed_teacher.rank == 4
        finish_tests = Todo.objects.get(title='Finish making tests')
        assert finish_tests.rank == 2

    def test_rerank_by_list(self):
        reranked = [todo.pk for todo in Todo.objects.all()]
        first = reranked[0]
        last = reranked.pop()
        reranked.insert(0, last)
        Todo.rerank_by_list(reranked)
        assert Todo.objects.get(pk=last).rank == 1
        assert Todo.objects.get(pk=first).rank == 2

    def test_empty_fields(self):
        empty_title = Todo.objects.filter(title='')[0]
        assert empty_title.text == 'What about a todo with no title?'

    def test_load_index(self):
        c = Client()
        response = c.get('/')
        self.assertTemplateUsed(response, 'todo/index.html')
        assert response.status_code == 200
        assert response['content-type'] == 'text/html; charset=utf-8'

    def test_load_item(self):
        feed_teacher = Todo.objects.get(title='Feed the teacher').pk
        c = Client()
        response = c.get(f'/todo_details_partial/{feed_teacher}/')
        assert b'Feed the teacher' in response.content

    def test_add_item(self):
        c = Client(HTTP_HX_CURRENT_URL='/')
        response = c.post(
            '/create/', {'title': 'Testing create function', 'text': 'Mandatory content'})
        assert response.status_code == 200
        response = c.get('/todo_details_partial/8/')
        assert b'Testing create function' in response.content
