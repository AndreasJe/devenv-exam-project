---
layout: default
---


**Welcome** to our user documentation for the KEA Todo app. Below you'll find a quick guide, that will show you how to use this little simple app.

[Documentation for developers](./dev-docs.html).

# How to make a todo:

![Step 1](./assets/img/user_doc/1.png)

As a new user, you won’t have many options. On the right pane, you’ll see two text fields for **Title** and  **Description**.

![Step 2](./assets/img/user_doc/2.png)

When you have filled out the two fields, and are satisfies with the results, you can press the **Submit** button

![Step 3](./assets/img/user_doc/3.png)

As the todo has been submitted, your page will refresh, and you’ll see your new todo on the left pane.

## How to change description or title?

![Step 4](./assets/img/user_doc/4.png)

Clicking on a todo, opens up a new window - this allows you to edit the information for you last todo.
Change the information and press **Update**.


## How to change description or title?

![Step 5](./assets/img/user_doc/4.png)

If you want to delete a Todo, you simple press on a todo on the left pane, you’ll be presented with a new panel.
Clicking on **Delete**, will delete the current todo.





```
That's all, folks!
```
