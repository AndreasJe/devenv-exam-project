
![header](./assets/img/githeader.png)


# Exam Project for Development Enviroments 2022

Introduction

## Getting started

Description

## How to run

- [ ] Move to workdir and run the following commands:
- [ ] Run: `Docker build .`
You should see the following output in your terminal:

```python
Sending build context to Docker daemon    983kB
Step 1/8 : FROM python:3.10-alpine
 ---> f7605eb83caf
Step 2/8 : ENV PYTHONUNBUFFERED=1
 ---> Using cache
 ---> 9df350abd14b
Step 3/8 : ENV PYTHONDONTWRITEBYTECODE=1
 ---> Using cache
 ---> a83430eeeb9b
Step 4/8 : RUN apk add python3-dev postgresql-client postgresql-dev musl-dev build-base
 ---> Using cache
 ---> d54afc738659
Step 5/8 : COPY . /app
 ---> b50e83b5abf8
Step 6/8 : WORKDIR /app
 ---> Running in e8142154609c
Removing intermediate container e8142154609c
 ---> 4d818dd2a1ad
Step 7/8 : RUN pip install -r /app/requirements.txt
 ---> Running in 240df0632561
Collecting asgiref==3.4.1
  Downloading asgiref-3.4.1-py3-none-any.whl (25 kB)
Collecting Django==4.0.4
  Downloading Django-4.0.4-py3-none-any.whl (8.0 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 8.0/8.0 MB 30.5 MB/s eta 0:00:00
Collecting pytz==2021.3
  Downloading pytz-2021.3-py2.py3-none-any.whl (503 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 503.5/503.5 KB 25.3 MB/s eta 0:00:00
Collecting sqlparse==0.4.2
  Downloading sqlparse-0.4.2-py3-none-any.whl (42 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 42.3/42.3 KB 6.6 MB/s eta 0:00:00
Collecting psycopg2-binary
  Downloading psycopg2_binary-2.9.3-cp310-cp310-musllinux_1_1_x86_64.whl (1.9 MB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.9/1.9 MB 40.9 MB/s eta 0:00:00
Collecting coverage==6.3.2
  Downloading coverage-6.3.2-cp310-cp310-musllinux_1_1_x86_64.whl (216 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 216.5/216.5 KB 16.8 MB/s eta 0:00:00
Collecting pip-audit==2.1.1
  Downloading pip_audit-2.1.1-py3-none-any.whl (72 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 72.9/72.9 KB 6.0 MB/s eta 0:00:00
Collecting cyclonedx-python-lib<3.0.0,>=1.0.0
  Downloading cyclonedx_python_lib-2.4.0-py3-none-any.whl (196 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 196.4/196.4 KB 11.8 MB/s eta 0:00:00
Collecting CacheControl[filecache]>=0.12.10
  Downloading CacheControl-0.12.11-py2.py3-none-any.whl (21 kB)
Collecting packaging>=21.0.0
  Downloading packaging-21.3-py3-none-any.whl (40 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 40.8/40.8 KB 10.4 MB/s eta 0:00:00
Collecting progress>=1.6
  Downloading progress-1.6.tar.gz (7.8 kB)
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting pip-api>=0.0.28
  Downloading pip_api-0.0.29-py3-none-any.whl (111 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 111.4/111.4 KB 7.2 MB/s eta 0:00:00
Collecting resolvelib>=0.8.0
  Downloading resolvelib-0.8.1-py2.py3-none-any.whl (16 kB)
Collecting html5lib>=1.1
  Downloading html5lib-1.1-py2.py3-none-any.whl (112 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 112.2/112.2 KB 8.1 MB/s eta 0:00:00
Collecting msgpack>=0.5.2
  Downloading msgpack-1.0.3.tar.gz (123 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 123.8/123.8 KB 8.2 MB/s eta 0:00:00
  Preparing metadata (setup.py): started
  Preparing metadata (setup.py): finished with status 'done'
Collecting requests
  Downloading requests-2.27.1-py2.py3-none-any.whl (63 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 63.1/63.1 KB 8.2 MB/s eta 0:00:00
Collecting lockfile>=0.9
  Downloading lockfile-0.12.2-py2.py3-none-any.whl (13 kB)
Requirement already satisfied: setuptools>=47.0.0 in /usr/local/lib/python3.10/site-packages (from cyclonedx-python-lib<3.0.0,>=1.0.0->pip-audit==2.1.1->-r /app/requirements.txt (line 7)) (58.1.0)
Collecting packageurl-python>=0.9
  Downloading packageurl_python-0.9.9-py3-none-any.whl (24 kB)
Collecting types-toml<0.11.0,>=0.10.0
  Downloading types_toml-0.10.7-py3-none-any.whl (4.5 kB)
Collecting toml<0.11.0,>=0.10.0
  Downloading toml-0.10.2-py2.py3-none-any.whl (16 kB)
Collecting types-setuptools>=57.0.0
  Downloading types_setuptools-57.4.17-py3-none-any.whl (27 kB)
Collecting webencodings
  Downloading webencodings-0.5.1-py2.py3-none-any.whl (11 kB)
Collecting six>=1.9
  Downloading six-1.16.0-py2.py3-none-any.whl (11 kB)
Collecting pyparsing!=3.0.5,>=2.0.2
  Downloading pyparsing-3.0.9-py3-none-any.whl (98 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 98.3/98.3 KB 13.2 MB/s eta 0:00:00
Requirement already satisfied: pip in /usr/local/lib/python3.10/site-packages (from pip-api>=0.0.28->pip-audit==2.1.1->-r /app/requirements.txt (line 7)) (22.0.4)
Collecting urllib3<1.27,>=1.21.1
  Downloading urllib3-1.26.9-py2.py3-none-any.whl (138 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 139.0/139.0 KB 17.1 MB/s eta 0:00:00
Collecting certifi>=2017.4.17
  Downloading certifi-2022.5.18.1-py3-none-any.whl (155 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 155.2/155.2 KB 10.3 MB/s eta 0:00:00
Collecting charset-normalizer~=2.0.0
  Downloading charset_normalizer-2.0.12-py3-none-any.whl (39 kB)
Collecting idna<4,>=2.5
  Downloading idna-3.3-py3-none-any.whl (61 kB)
     ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 61.2/61.2 KB 13.2 MB/s eta 0:00:00
Building wheels for collected packages: progress, msgpack
  Building wheel for progress (setup.py): started
  Building wheel for progress (setup.py): finished with status 'done'
  Created wheel for progress: filename=progress-1.6-py3-none-any.whl size=9632 sha256=2a8614002e19c96ae9e5c09a098be48483f1949784105047ca2c207869f29b43
  Stored in directory: /root/.cache/pip/wheels/a2/68/5f/c339b20a41659d856c93ccdce6a33095493eb82c3964aac5a1
  Building wheel for msgpack (setup.py): started
  Building wheel for msgpack (setup.py): still running...
  Building wheel for msgpack (setup.py): finished with status 'done'
  Created wheel for msgpack: filename=msgpack-1.0.3-cp310-cp310-linux_x86_64.whl size=78637 sha256=83d68f09d115ed57dcdfacb17ae698eaef29cbffcd57063282806fbc74732b3e
  Stored in directory: /root/.cache/pip/wheels/8c/f8/b1/9bba738952736d397da6b1efcbe75f92026c2d85c981e67d05
Successfully built progress msgpack
Installing collected packages: webencodings, types-toml, types-setuptools, resolvelib, pytz, progress, msgpack, lockfile, urllib3, toml, sqlparse, six, pyparsing, psycopg2-binary, pip-api, packageurl-python, idna, coverage, charset-normalizer, certifi, asgiref, requests, packaging, html5lib, Django, cyclonedx-python-lib, CacheControl, pip-audit
Successfully installed CacheControl-0.12.11 Django-4.0.4 asgiref-3.4.1 certifi-2022.5.18.1 charset-normalizer-2.0.12 coverage-6.3.2 cyclonedx-python-lib-2.4.0 html5lib-1.1 idna-3.3 lockfile-0.12.2 msgpack-1.0.3 packageurl-python-0.9.9 packaging-21.3 pip-api-0.0.29 pip-audit-2.1.1 progress-1.6 psycopg2-binary-2.9.3 pyparsing-3.0.9 pytz-2021.3 requests-2.27.1 resolvelib-0.8.1 six-1.16.0 sqlparse-0.4.2 toml-0.10.2 types-setuptools-57.4.17 types-toml-0.10.7 urllib3-1.26.9 webencodings-0.5.1
WARNING: Running pip as the 'root' user can result in broken permissions and conflicting behaviour with the system package manager. It is recommended to use a virtual environment instead: https://pip.pypa.io/warnings/venv
WARNING: You are using pip version 22.0.4; however, version 22.1.1 is available.
You should consider upgrading via the '/usr/local/bin/python -m pip install --upgrade pip' command.
Removing intermediate container 240df0632561
 ---> 44d1dc932ca9
Step 8/8 : ENTRYPOINT ["sh", "entrypoint.sh"]
 ---> Running in 5d70038adfe8
Removing intermediate container 5d70038adfe8
 ---> d6846d17832a
Successfully built d6846d17832a

```

- [ ] Run: `Docker-compose up`

You should see the following output in your terminal:


```python
Creating network "devenv-exam-project_default" with the default driver
Creating devenv-exam-project_db_1 ... done
Creating devenv-exam-project_app_1 ... done
Creating devenv-exam-project_nginx_1 ... done
Attaching to devenv-exam-project_db_1, devenv-exam-project_app_1, devenv-exam-project_nginx_1
db_1     | 
db_1     | PostgreSQL Database directory appears to contain a database; Skipping initialization
db_1     | 
db_1     | 2022-05-30 23:20:54.284 UTC [1] LOG:  starting PostgreSQL 14.2 on x86_64-pc-linux-musl, compiled by gcc (Alpine 10.3.1_git20211027) 10.3.1 20211027, 64-bit
db_1     | 2022-05-30 23:20:54.288 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
db_1     | 2022-05-30 23:20:54.288 UTC [1] LOG:  listening on IPv6 address "::", port 5432
db_1     | 2022-05-30 23:20:54.290 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
db_1     | 2022-05-30 23:20:54.313 UTC [21] LOG:  database system was shut down at 2022-05-29 13:56:16 UTC
db_1     | 2022-05-30 23:20:54.327 UTC [1] LOG:  database system is ready to accept connections
nginx_1  | /docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
nginx_1  | /docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
nginx_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
nginx_1  | 10-listen-on-ipv6-by-default.sh: info: /etc/nginx/conf.d/default.conf is not a file or does not exist
nginx_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
nginx_1  | /docker-entrypoint.sh: Launching /docker-entrypoint.d/30-tune-worker-processes.sh
nginx_1  | /docker-entrypoint.sh: Configuration complete; ready for start up
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: using the "epoll" event method
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: nginx/1.21.6
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: built by gcc 10.2.1 20210110 (Debian 10.2.1-6) 
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: OS: Linux 5.15.23-0-virt
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: getrlimit(RLIMIT_NOFILE): 1048576:1048576
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: start worker processes
nginx_1  | 2022/05/30 23:20:56 [notice] 1#1: start worker process 22
app_1    | System check identified no issues (0 silenced).
app_1    | No changes detected
app_1    | Operations to perform:
app_1    |   Apply all migrations: admin, auth, contenttypes, sessions, todo
app_1    | Running migrations:
app_1    |   No migrations to apply.
app_1    | ** Development mode.
Found 8 known vulnerabilities in 1 package
app_1    | Name   Version ID             Fix Versions
app_1    | ------ ------- -------------- --------------------
app_1    | django 3.2.9   PYSEC-2022-2   2.2.26,3.2.11,4.0.1
app_1    | django 3.2.9   PYSEC-2021-439 2.2.25,3.1.14,3.2.10
app_1    | django 3.2.9   PYSEC-2022-3   2.2.26,3.2.11,4.0.1
app_1    | django 3.2.9   PYSEC-2022-1   2.2.26,3.2.11,4.0.1
app_1    | django 3.2.9   PYSEC-2022-20  2.2.27,3.2.12,4.0.2
app_1    | django 3.2.9   PYSEC-2022-19  2.2.27,3.2.12,4.0.2
app_1    | django 3.2.9   PYSEC-2022-190 2.2.28,3.2.13,4.0.4
app_1    | django 3.2.9   PYSEC-2022-191 2.2.28,3.2.13,4.0.4
app_1    | Creating test database for alias 'default' ('test_todo')...
app_1    | Operations to perform:
app_1    |   Synchronize unmigrated apps: messages, staticfiles
app_1    |   Apply all migrations: admin, auth, contenttypes, sessions, todo
app_1    | Synchronizing apps without migrations:
app_1    |   Creating tables...
app_1    |     Running deferred SQL...
app_1    | Running migrations:
app_1    |   Applying contenttypes.0001_initial... OK
app_1    |   Applying auth.0001_initial... OK
app_1    |   Applying admin.0001_initial... OK
app_1    |   Applying admin.0002_logentry_remove_auto_add... OK
app_1    |   Applying admin.0003_logentry_add_action_flag_choices... OK
app_1    |   Applying contenttypes.0002_remove_content_type_name... OK
app_1    |   Applying auth.0002_alter_permission_name_max_length... OK
app_1    |   Applying auth.0003_alter_user_email_max_length... OK
app_1    |   Applying auth.0004_alter_user_username_opts... OK
app_1    |   Applying auth.0005_alter_user_last_login_null... OK
app_1    |   Applying auth.0006_require_contenttypes_0002... OK
app_1    |   Applying auth.0007_alter_validators_add_error_messages... OK
app_1    |   Applying auth.0008_alter_user_username_max_length... OK
app_1    |   Applying auth.0009_alter_user_last_name_max_length... OK
app_1    |   Applying auth.0010_alter_group_name_max_length... OK
app_1    |   Applying auth.0011_update_proxy_permissions... OK
app_1    |   Applying auth.0012_alter_user_first_name_max_length... OK
app_1    |   Applying sessions.0001_initial... OK
app_1    |   Applying todo.0001_initial... OK
app_1    | System check identified no issues (0 silenced).
app_1    | test_add_item (todo.tests.TodoTestCase) ... ok
app_1    | test_default_order (todo.tests.TodoTestCase) ... ok
app_1    | test_empty_fields (todo.tests.TodoTestCase) ... ok
app_1    | test_highest_rank (todo.tests.TodoTestCase) ... ok
app_1    | test_load_index (todo.tests.TodoTestCase) ... ok
app_1    | test_load_item (todo.tests.TodoTestCase) ... ok
app_1    | test_rerank_by_list (todo.tests.TodoTestCase) ... ok
app_1    | test_rerank_down (todo.tests.TodoTestCase) ... ok
app_1    | test_rerank_up (todo.tests.TodoTestCase) ... ok
app_1    | 
app_1    | ----------------------------------------------------------------------
app_1    | Ran 9 tests in 0.634s
app_1    | 
app_1    | OK
app_1    | Destroying test database for alias 'default' ('test_todo')...
app_1    | Name                             Stmts   Miss  Cover   Missing
app_1    | --------------------------------------------------------------
app_1    | http_put_parsing/apps.py             4      4     0%   1-6
app_1    | http_put_parsing/middleware.py      19     12    37%   9-21
app_1    | http_put_parsing/tests.py            0      0   100%
app_1    | project/settings.py                 20      0   100%
app_1    | project/urls.py                      3      0   100%
app_1    | todo/admin.py                        9      0   100%
app_1    | todo/apps.py                         4      0   100%
app_1    | todo/models.py                      43      5    88%   13, 30, 54, 61-62
app_1    | todo/tests.py                       60      0   100%
app_1    | todo/urls.py                         4      0   100%
app_1    | todo/views.py                       29     13    55%   11-12, 27-35, 39-41
app_1    | --------------------------------------------------------------
app_1    | TOTAL                              195     34    83%
app_1    | Watching for file changes with StatReloader
app_1    | Performing system checks...
app_1    | 
app_1    | System check identified no issues (0 silenced).
app_1    | May 30, 2022 - 23:21:34
app_1    | Django version 3.2.9, using settings 'project.settings'
app_1    | Starting development server at http://0.0.0.0:8000/
app_1    | Quit the server with CONTROL-C.

```

To stop the container simply press `CTRL+C`.
Removing any leftover containers running, can be done with `docker-compose down` 





## Requirements


Dependencies are found in the [Requirements.txt](https://gitlab.com/AndreasJe//blob/main/requirements.txt)

## Team

- [Nikolai Baida](https://gitlab.com/niko856)
- [Victor Øbro Bucholdtz](https://gitlab.com/vict889f)
- [Andreas Jensen](https://gitlab.com/AndreasJe)
- [Asta Vittrup Graversen](https://gitlab.com/asta3090)
